#pragma once

#include <exception>
#include <pcre2posix.h>

class Pcre2Exception : public std::exception {
public:
    Pcre2Exception(int error_code, const regex_t* regex = nullptr);
    const char* what() const noexcept;

private:
    char _error_message[1024];
};

class Pcre2Regex {
public:
    // https://stackoverflow.com/a/2173764
    Pcre2Regex(const Pcre2Regex&) = delete;
    Pcre2Regex& operator=(const Pcre2Regex&) = delete;

    Pcre2Regex(const char* pattern, int cflags = 0);
    ~Pcre2Regex();
    bool match(const char* str, size_t nmatch, regmatch_t pmatch[], int eflags = 0) const;

private:
    regex_t _regex;
};
