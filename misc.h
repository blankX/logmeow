#pragma once

#include <string>
typedef int ImGuiWindowFlags; // forward declaration from imgui/imgui.h
typedef int ImGuiKeyChord; // forward declaration from imgui/imgui.h
typedef int ImGuiDir; // forward declaration from imgui/imgui.h

std::string quote(const std::string& str);
void throw_system_error(int err, const char* what);
void throw_system_error(const char* what);
void throw_system_error(std::string what);

namespace ImGui {
    void TextUnformatted(const std::string& str);
    bool RedButton(const char* label);
    bool Button(const char* label, bool enabled);
    bool ArrowButton(const char* label, ImGuiDir dir, bool enabled);
    bool BeginWithCloseShortcut(const char* label, bool* p_open, ImGuiWindowFlags flags = 0);
    // don't spill __all__ of imgui_internal.h
    bool IsKeyPressed(ImGuiKeyChord key_chord, bool repeat = true);
}; // namespace ImGui
