#include "pcre2_wrapper.h"

Pcre2Exception::Pcre2Exception(int error_code, const regex_t* regex) {
    pcre2_regerror(error_code, regex, this->_error_message, 1024);
}

const char* Pcre2Exception::what() const noexcept {
    return this->_error_message;
}

Pcre2Regex::Pcre2Regex(const char* pattern, int cflags) {
    int res = pcre2_regcomp(&this->_regex, pattern, cflags);
    if (res) {
        throw Pcre2Exception(res);
    }
}

Pcre2Regex::~Pcre2Regex() {
    pcre2_regfree(&this->_regex);
}

bool Pcre2Regex::match(const char* str, size_t nmatch, regmatch_t pmatch[], int eflags) const {
    int res = pcre2_regexec(&this->_regex, str, nmatch, pmatch, eflags);
    if (res == REG_NOMATCH) {
        return false;
    }
    if (res) {
        throw Pcre2Exception(res, &this->_regex);
    }
    return true;
}
