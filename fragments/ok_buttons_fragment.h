#pragma once

#include <imgui/imgui.h>
namespace ImGui { bool IsKeyPressed(ImGuiKeyChord key_chord, bool repeat); }; // forward declaration from ../misc.h

void ok_buttons_fragment(bool* p_open, auto ok, auto apply);

// function bodies must be in the header because https://stackoverflow.com/a/999383

void ok_buttons_fragment(bool* p_open, auto ok, auto apply) {
    bool action_done = false;
    ImVec2 button_size(4 * ImGui::GetFontSize(), 0);
    bool ctrl_s_pressed = ImGui::IsKeyPressed(ImGuiMod_Shortcut | ImGuiKey_S);

    if (ImGui::Button("OK", button_size) && !action_done) {
        ok();
        *p_open = false;
        action_done = true;
    }

    ImGui::SameLine();
    if (ImGui::Button("Cancel", button_size) && !action_done) {
        *p_open = false;
        action_done = true;
    }

    ImGui::SameLine();
    if ((ImGui::Button("Apply", button_size) || ctrl_s_pressed) && !action_done) {
        apply();
        action_done = true;
    }
}
