#pragma once

class LogcatEntries; // forward declaration from ../logcat_entry.h

void export_fragment(const LogcatEntries& logcat_entries);
