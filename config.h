#pragma once

#include <string>
#include <nlohmann/json.hpp>
#include "filters.h"

const constexpr char* default_logcat_command = "adb logcat -Dv 'threadtime epoch UTC uid'";

struct Config {
    std::string logcat_command;
    float normal_font_size = 13.0f;
    float monospace_font_size = 13.0f;

    Filters filters;
    Filters exclusions;
};

std::string get_config_folder();
void create_config_folders_if_necessary();
void from_json(const nlohmann::json& j, Config& config);
Config load_config();
void to_json(nlohmann::json& j, const Config& config);
void write_config(const Config& config);
