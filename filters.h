#pragma once

#include <string>
#include <vector>
#include <memory>
#include <utility>
#include <nlohmann/json.hpp>

#include "logcat_entry.h"
#include "pcre2_wrapper.h"

enum class FilterKey {
    Buffer,
    User,
    PID,
    TID,
    Priority,
    Tag,
    Message,
};

class Filter {
public:
    // https://stackoverflow.com/a/10024812
    virtual ~Filter() = 0;

    virtual void updated() = 0;
    virtual bool disabled() const = 0;
    virtual void disabled(bool disabled) = 0;
    // https://stackoverflow.com/a/65344294
    virtual std::unique_ptr<Filter> clone() const = 0;
    virtual const std::string* error() const = 0;
    virtual bool match(const LogcatEntry& entry) const = 0;
};

class IntegerFilter : public Filter {
public:
    IntegerFilter(FilterKey key_, size_t other_, bool inverted_, bool disabled);

    void updated() override;
    bool disabled() const override;
    void disabled(bool disabled) override;
    std::unique_ptr<Filter> clone() const override;
    const std::string* error() const override;
    bool match(const LogcatEntry& entry) const override;

    FilterKey key;
    size_t other;
    bool inverted;

private:
    bool _disabled;
};

class StringFilter : public Filter {
public:
    // https://stackoverflow.com/a/2173764
    StringFilter(const StringFilter&) = delete;
    StringFilter& operator=(const StringFilter&) = delete;

    StringFilter(FilterKey key_, std::string other_, bool inverted_, bool exact_match_, bool disabled);

    void updated() override;
    bool disabled() const override;
    void disabled(bool disabled) override;
    std::unique_ptr<Filter> clone() const override;
    const std::string* error() const override;
    bool match(const LogcatEntry& entry) const override;

    FilterKey key;
    std::string other;
    bool inverted;
    bool exact_match;

private:
    bool _match_no_regex(const LogcatEntry& entry) const;
    bool _match_regex(const LogcatEntry& entry) const;

    std::string _error;
    std::optional<Pcre2Regex> _regex;
    bool _disabled;
};

class BufferFilter : public Filter {
public:
    BufferFilter(unsigned int wanted_, bool disabled);

    void updated() override;
    bool disabled() const override;
    void disabled(bool disabled) override;
    std::unique_ptr<Filter> clone() const override;
    const std::string* error() const override;
    bool match(const LogcatEntry& entry) const override;

    unsigned int wanted;

private:
    bool _disabled;
};

class PriorityFilter : public Filter {
public:
    PriorityFilter(unsigned int wanted_, bool disabled = false);

    void updated() override;
    bool disabled() const override;
    void disabled(bool disabled) override;
    std::unique_ptr<Filter> clone() const override;
    const std::string* error() const override;
    bool match(const LogcatEntry& entry) const override;

    unsigned int wanted;

private:
    bool _disabled;
};

class GroupFilter : public Filter {
public:
    enum class Type {
        All = 0,
        Any = 1,
        One = 2,
        None = 3,
    };

    GroupFilter(std::vector<std::unique_ptr<Filter>> filters_, Type type_, bool disabled);

    void updated() override;
    bool disabled() const override;
    void disabled(bool disabled) override;
    std::unique_ptr<Filter> clone() const override;
    const std::string* error() const override;
    bool match(const LogcatEntry& entry) const override;

    std::vector<std::unique_ptr<Filter>> filters;
    Type type;

private:
    bool _disabled;
};

typedef std::vector<std::pair<std::string, std::unique_ptr<Filter>>> Filters;
void copy_filters(Filters& __restrict filters, const Filters& __restrict other);
bool matches(const LogcatEntry& entry, const Filters& __restrict filters, const Filters& __restrict exclusions);

void from_json(const nlohmann::json& j, FilterKey& key);
void to_json(nlohmann::json& j, const FilterKey& key);
void from_json(const nlohmann::json& j, GroupFilter::Type& type);
void to_json(nlohmann::json& j, const GroupFilter::Type& type);
void from_json(const nlohmann::json& j, std::unique_ptr<Filter>& filter);
void to_json(nlohmann::json& j, const std::unique_ptr<Filter>& filter);
