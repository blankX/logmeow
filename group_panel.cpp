#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui/imgui.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <imgui/imgui_internal.h>
#pragma GCC diagnostic pop
#include "group_panel.h"

// The following code is slightly modified public domain code from https://github.com/thedmd
// All modifications have an inline comment with "[CUSTOM]"
// Public domain claim from https://github.com/ocornut/imgui/issues/1496#issuecomment-1287772456
// Code from https://github.com/ocornut/imgui/issues/1496#issuecomment-569892444 and
// some snippets from https://github.com/ocornut/imgui/issues/1496#issuecomment-655048353

void ImGui::BeginGroupPanel(const ImVec2& size) // [CUSTOM]
{
    ImGui::BeginGroup();

    //auto cursorPos = ImGui::GetCursorScreenPos(); // [CUSTOM]
    auto itemSpacing = ImGui::GetStyle().ItemSpacing;
    ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0.0f, 0.0f));
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0f, 0.0f));

    auto frameHeight = ImGui::GetFrameHeight();
    ImGui::BeginGroup();

    ImVec2 effectiveSize = size;
    if (size.x < 0.0f)
        effectiveSize.x = ImGui::GetContentRegionAvail().x; // [CUSTOM]
    else
        effectiveSize.x = size.x;
    ImGui::Dummy(ImVec2(effectiveSize.x, 0.0f));

    ImGui::Dummy(ImVec2(frameHeight, 0.0f)); // [CUSTOM]
    ImGui::SameLine(0.0f, 0.0f);
    ImGui::BeginGroup();
    ImGui::Dummy(ImVec2(frameHeight, 0.0f)); // [CUSTOM]
    ImGui::SameLine(0.0f, 0.0f);
    //ImGui::TextUnformatted(name); // [CUSTOM]
    ImGui::SameLine(0.0f, 0.0f);
    ImGui::Dummy(ImVec2(0.0, frameHeight + itemSpacing.y));
    ImGui::BeginGroup();

    ImGui::PopStyleVar(2);

    //ImGui::GetCurrentWindow()->ContentsRegionRect.Max.x -= frameHeight * 0.5f; // [CUSTOM]
    ImGui::GetCurrentWindow()->ContentRegionRect.Max.x -= frameHeight * 0.5f;
    ImGui::GetCurrentWindow()->WorkRect.Max.x          -= frameHeight * 0.5f;
    ImGui::GetCurrentWindow()->InnerRect.Max.x         -= frameHeight * 0.5f;
    ImGui::GetCurrentWindow()->Size.x                   -= frameHeight;

    auto itemWidth = ImGui::CalcItemWidth();
    ImGui::PushItemWidth(ImMax(0.0f, itemWidth - frameHeight));
    //ImGui::PushItemWidth(effectiveSize.x - frameHeight); // [CUSTOM]
}

void ImGui::EndGroupPanel()
{
    ImGui::PopItemWidth();

    auto itemSpacing = ImGui::GetStyle().ItemSpacing;

    ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0.0f, 0.0f));
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0f, 0.0f));

    auto frameHeight = ImGui::GetFrameHeight();

    ImGui::EndGroup();

    //ImGui::GetWindowDrawList()->AddRectFilled(ImGui::GetItemRectMin(), ImGui::GetItemRectMax(), IM_COL32(0, 255, 0, 64), 4.0f);

    ImGui::EndGroup();

    ImGui::SameLine(0.0f, 0.0f);
    ImGui::Dummy(ImVec2(frameHeight * 0.5f, 0.0f));
    ImGui::Dummy(ImVec2(0.0, frameHeight - itemSpacing.y)); // [CUSTOM]

    ImGui::EndGroup();

    auto itemMin = ImGui::GetItemRectMin();
    auto itemMax = ImGui::GetItemRectMax();
    //ImGui::GetWindowDrawList()->AddRectFilled(itemMin, itemMax, IM_COL32(255, 0, 0, 64), 4.0f);

    ImVec2 halfFrame = ImVec2(frameHeight * 0.25f, frameHeight) * 0.5f;
    ImGui::GetWindowDrawList()->AddRect(
        itemMin + halfFrame, itemMax - ImVec2(halfFrame.x, 0.0f),
        ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Border)),
        halfFrame.x);

    ImGui::PopStyleVar(2);

    //ImGui::GetCurrentWindow()->ContentsRegionRect.Max.x += frameHeight * 0.5f; // [CUSTOM]
    ImGui::GetCurrentWindow()->ContentRegionRect.Max.x += frameHeight * 0.5f;
    ImGui::GetCurrentWindow()->WorkRect.Max.x          += frameHeight * 0.5f;
    ImGui::GetCurrentWindow()->InnerRect.Max.x         += frameHeight * 0.5f;
    ImGui::GetCurrentWindow()->Size.x                   += frameHeight;

    ImGui::Dummy(ImVec2(0.0f, 0.0f));

    ImGui::EndGroup();
}
