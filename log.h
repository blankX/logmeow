#pragma once

#include <ctime>
#include <string>
#include <vector>

struct LogEntry {
    struct tm time;
    std::string message;

    LogEntry() = default;
    LogEntry(time_t time_, std::string message_);
};
extern std::vector<LogEntry> log_entries;

std::string to_string(const LogEntry& entry);
void print_log(const LogEntry& entry);
void log(LogEntry entry, bool print = true);
void log(std::string entry);
