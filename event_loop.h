#pragma once

struct ImFont; // forward declaration from imgui/imgui.h

struct Config; // forward declaration from config.h
class LogcatThread; // forward declaration from logcat_thread.h

void event_loop(ImFont* monospace_font, Config& active_config, LogcatThread& logcat_thread);
