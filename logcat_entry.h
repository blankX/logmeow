#pragma once

#include <ctime>
#include <string>
#include <optional>
#include <nlohmann/json.hpp>

struct Config; // forward declaration from config.h

#define MAX_LOGCAT_LINE_SIZE 512 * 1024

enum class Buffer {
    Unknown = 0b1,
    Main = 0b10,
    System = 0b100,
    Radio = 0b1000,
    Events = 0b10000,
    Crash = 0b100000,
};

enum class Priority {
    Unknown = 0b1,
    Verbose = 0b10,
    Debug = 0b100,
    Info = 0b1000,
    Warn = 0b10000,
    Error = 0b100000,
    Fatal = 0b1000000,
};

struct LogcatEntry {
    Buffer buffer;
    struct tm time;
    std::optional<std::string> user;
    size_t pid;
    size_t tid;
    Priority priority;
    std::string tag;
    std::string message;
};

class LogcatEntries {
public:
    // https://stackoverflow.com/a/2173764
    LogcatEntries(const LogcatEntries&) = delete;
    LogcatEntries& operator=(const LogcatEntries&) = delete;

    LogcatEntries(const Config& config) : _config(config) {}

    inline constexpr size_t size_all() const noexcept {
        return this->_logcat_entries.size();
    }
    inline constexpr size_t size_filtered() const noexcept {
        return this->_filtered_offsets.size();
    }
    inline constexpr void clear() noexcept {
        this->_filtered_offsets.clear();
        this->_logcat_entries.clear();
    }

    inline constexpr const LogcatEntry& at_all(size_t index) const noexcept {
        return this->_logcat_entries[index];
    }
    inline constexpr const LogcatEntry& at_filtered(size_t index) const noexcept {
        return this->_logcat_entries[this->_filtered_offsets[index]];
    }

    void push_back(LogcatEntry entry);
    void refresh();

private:
    std::vector<LogcatEntry> _logcat_entries;
    std::vector<size_t> _filtered_offsets;
    const Config& _config;
};

const char* to_string(Priority priority);
const char* to_string(Buffer buffer);
const char* to_string_lower(Buffer buffer);
std::string to_string(const LogcatEntry& logcat_entry);
std::optional<LogcatEntry> try_parse_logcat_entry(char* buf, size_t length, Buffer buffer);
std::optional<Buffer> try_parse_buffer(char* buf, size_t length);

void from_json(const nlohmann::json& j, Buffer& buffer);
void to_json(nlohmann::json& j, const Buffer& buffer);
void from_json(const nlohmann::json& j, Priority& priority);
void to_json(nlohmann::json& j, const Priority& priority);
