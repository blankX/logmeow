#pragma once

struct ImFont; // forward declaration from imgui/imgui.h
#include <vector>

struct Config; // forward declaration from ../config.h
class LogcatThread; // forward declaration from ../logcat_thread.h
class LogcatEntries; // forward declaration from ../logcat_entry.h

void main_window(bool latest_log_entries_read, ImFont* monospace_font, LogcatThread& logcat_thread, LogcatEntries& logcat_entries,
        const Config& __restrict active_config, Config& __restrict inactive_config,
        bool* __restrict show_settings_window, bool* __restrict show_logs_window);
