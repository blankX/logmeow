#pragma once

#include <vector>

class LogcatEntries; // forward declaration from ../logcat_entry.h
class LogcatThread; // forward declaration from ../logcat_thread.h

void debug_window(LogcatThread& logcat_thread, LogcatEntries& logcat_entries);
