#pragma once

struct ImFont; // forward declaration from imgui/imgui.h

void logs_window(ImFont* monospace_font, bool* __restrict autoscrolling, bool* __restrict p_open);
