#pragma once

#include <vector>
struct ImFont; // forward declaration from imgui/imgui.h

struct Config; // forward declaration from ../config.h
class LogcatEntries; // forward declaration from ../logcat_entry.h

void settings_window(ImFont* monospace_font, Config& __restrict active_config, Config& __restrict inactive_config,
    LogcatEntries& logcat_entries, bool* p_open);
