#include <imgui/imgui.h>
#include <imgui/misc/cpp/imgui_stdlib.h>

#include "../misc.h"
#include "../config.h"
#include "../fragments/filters.h"
#include "../fragments/ok_buttons_fragment.h"
#include "settings.h"

static void try_write_config(const Config& config) {
    try {
        write_config(config);
    } catch (const std::exception& e) {
        log(std::string("Failed to write config: ") + e.what());
    }
}

static inline void settings_fragment(ImFont* monospace_font, Config& inactive_config) {
    ImGui::TextUnformatted("Logcat command only takes effect when logcat is not running");
    ImGui::PushFont(monospace_font);
    ImGui::InputTextWithHint("##logcat_command", default_logcat_command, &inactive_config.logcat_command);
    ImGui::PopFont();
    ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
    ImGui::TextUnformatted("Logcat command");

    ImGui::TextWrapped("Font sizes only take effect when LogMeow is restarted (using non-integer values may cause spacing issues)");
#ifdef USE_FONTCONFIG
    ImGui::InputFloat("Normal font size", &inactive_config.normal_font_size, 0.5f, 1.0f, "%.3f");
#endif
    ImGui::InputFloat("Monospace font size", &inactive_config.monospace_font_size, 0.5f, 1.0f, "%.3f");
}

void settings_window(ImFont* monospace_font, Config& __restrict active_config, Config& __restrict inactive_config,
        LogcatEntries& logcat_entries, bool* p_open) {
    if (!ImGui::BeginWithCloseShortcut("Settings", p_open)) {
        ImGui::End();
        return;
    }

    if (ImGui::BeginTabBar("settings")) {
        if (ImGui::BeginTabItem("Settings")) {
            settings_fragment(monospace_font, inactive_config);
            ImGui::EndTabItem();
        }
        if (ImGui::BeginTabItem("Filters")) {
            filters_fragment(inactive_config.filters);
            ImGui::EndTabItem();
        }
        if (ImGui::BeginTabItem("Exclusions")) {
            filters_fragment(inactive_config.exclusions);
            ImGui::EndTabItem();
        }
        ImGui::EndTabBar();
    }

    ImGui::Separator();
    ok_buttons_fragment(p_open, [&]() {
        active_config = std::move(inactive_config);
        logcat_entries.refresh();
        try_write_config(active_config);
    }, [&]() {
        active_config.logcat_command = inactive_config.logcat_command;
        active_config.normal_font_size = inactive_config.normal_font_size;
        active_config.monospace_font_size = inactive_config.monospace_font_size;
        copy_filters(active_config.filters, inactive_config.filters);
        copy_filters(active_config.exclusions, inactive_config.exclusions);
        logcat_entries.refresh();
        try_write_config(active_config);
    });
    ImGui::End();
}
