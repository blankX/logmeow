#include <imgui/imgui.h>
#include "../myimconfig.h"

#include "../log.h"
#include "../logcat_entry.h"
#include "../logcat_thread.h"
#include "debug.h"

void debug_window(LogcatThread& logcat_thread, LogcatEntries& logcat_entries) {
    static bool show_demo_window = false;
    static size_t add_log_entry_presses = 1;
    static bool log_entry_every_second = false;
    static float log_entry_every_second_delta;

    if (show_demo_window) {
        ImGui::ShowDemoWindow(&show_demo_window);
    }
    if (!ImGui::Begin("LogMeow Debug")) {
        ImGui::End();
        return;
    }

    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    ImGui::Checkbox("Show Dear ImGui Demo Window", &show_demo_window);

    ImGui::Separator();
    if (ImGui::Button("Add Log Entry")) {
        log(std::string("Debug log entry #") + std::to_string(add_log_entry_presses++) + " (activated via manual button press)");
    }
    ImGui::SameLine();
    // returns true when it's pressed
    if (ImGui::Checkbox("Add log entry every second", &log_entry_every_second)) {
        log_entry_every_second_delta = 0.0f;
    }
    if (ImGui::Button("Add Log Entry with Newlines")) {
        log("The following should have five spaces: \"\n\n\n\n\n\"");
    }
    if (ImGui::Button("Test user assert")) {
        IM_ASSERT_USER_ERROR(0, "User assert tested");
    }
    if (ImGui::Button("Request log entry from Logcat thread")) {
        logcat_thread.debug_log_request.test_and_set();
    }

    if (log_entry_every_second) {
        log_entry_every_second_delta += ImGui::GetIO().DeltaTime;
        if (log_entry_every_second_delta >= 1.0f) {
            log_entry_every_second_delta = 0.0f;
            log(std::string("Debug log entry #") + std::to_string(add_log_entry_presses++) + " (activated by add log entry every second)");
        }
    }

    ImGui::Separator();
    if (ImGui::Button("Add test entry (w/ user)")) {
        LogcatEntry entry = {
            .buffer = Buffer::Main,
            // time to be set later
            .user = "blankie",
            .pid = 69,
            .tid = 420,
            .priority = Priority::Error,
            .tag = "blanket, inc.",
            .message = "Failed to make blanket",
        };
        time_t entry_time = time(nullptr);
        localtime_r(&entry_time, &entry.time);
        logcat_entries.push_back(std::move(entry));
    }
    if (ImGui::Button("Add test entry (w/o user)")) {
        LogcatEntry entry = {
            .buffer = Buffer::Crash,
            // time to be set later
            .pid = 420,
            .tid = 69,
            .priority = Priority::Fatal,
            .tag = "blanket, inc.",
            .message = "Failed to invent blankets",
        };
        time_t entry_time = time(nullptr);
        localtime_r(&entry_time, &entry.time);
        logcat_entries.push_back(std::move(entry));
    }

    ImGui::End();
}
