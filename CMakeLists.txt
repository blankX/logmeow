cmake_minimum_required(VERSION 3.25)

project(logmeow CXX)

## ---------------------------------------------------------------------
## GENERIC BUILD FLAGS
## ---------------------------------------------------------------------

find_package(SDL2 REQUIRED)
find_package(Freetype 2 REQUIRED)
find_package(nlohmann_json REQUIRED)
list(APPEND LIBS -lpcre2-8 -lpcre2-posix SDL2 nlohmann_json::nlohmann_json imgui nfd)
list(APPEND IMGUI_LIBS SDL2 Freetype::Freetype)

set(INCLUDES "")
set(SOURCES "")
set(IMGUI_SOURCES "")
set(DEFINITIONS "")
# imgui include because https://github.com/ocornut/imgui/issues/6184#issuecomment-1439570929
list(APPEND INCLUDES thirdparty thirdparty/imgui /usr/include/SDL2)
list(APPEND SOURCES main.cpp event_loop.cpp logcat_thread.cpp logcat_entry.cpp log.cpp config.cpp filters.cpp misc.cpp pcre2_wrapper.cpp
    group_panel.cpp fragments/filters.cpp fragments/export.cpp fragments/import.cpp windows/logs.cpp windows/settings.cpp windows/main.cpp)
list(APPEND IMGUI_SOURCES thirdparty/imgui/imgui.cpp thirdparty/imgui/imgui_draw.cpp thirdparty/imgui/imgui_widgets.cpp thirdparty/imgui/imgui_tables.cpp
    thirdparty/imgui/misc/cpp/imgui_stdlib.cpp thirdparty/imgui/misc/freetype/imgui_freetype.cpp
    thirdparty/imgui/backends/imgui_impl_sdl2.cpp thirdparty/imgui/backends/imgui_impl_opengl3.cpp)
list(APPEND DEFINITIONS -DIMGUI_USER_CONFIG="../../myimconfig.h")

if (CMAKE_BUILD_TYPE MATCHES "Debug")
    list(APPEND SOURCES windows/debug.cpp)
    list(APPEND IMGUI_SOURCES thirdparty/imgui/imgui_demo.cpp)
    if (NOT FLAGS)
        list(APPEND FLAGS -fsanitize=undefined,thread)
    endif()
    # https://sourceforge.net/p/valgrind/mailman/valgrind-users/thread/Ygze8PzaQAYWlKDj%40wildebeest.org/
    list(APPEND FLAGS -gdwarf-4)
endif()

# https://t.me/NightShadowsHangout/670691
# https://t.me/NightShadowsHangout/688372
list(APPEND FLAGS -Werror -Wall -Wextra -Wshadow -Wpedantic -Wno-gnu-anonymous-struct -fPIC -Wconversion -Wno-unused-parameter -Wimplicit-fallthrough -Wno-missing-field-initializers)

# i have no idea why this hack wasn't needed before but it's needed if sanitizers are used
add_link_options(${FLAGS})

## ---------------------------------------------------------------------
## OPENGL ES
## ---------------------------------------------------------------------

set(LINUX_GL_LIBS -lGL)

## This assumes a GL ES library available in the system, e.g. libGLESv2.so
# list(APPEND FLAGS -DIMGUI_IMPL_OPENGL_ES2)
# set(LINUX_GL_LIBS -lGLESv2)
## If you're on a Raspberry Pi and want to use the legacy drivers,
## use the following instead:
# set(LINUX_GL_LIBS -L/opt/vc/lib -lbrcmGLESv2)

## ---------------------------------------------------------------------
## BUILD FLAGS PER PLATFORM
## ---------------------------------------------------------------------

if (LINUX)
    find_package(Fontconfig)
    list(APPEND LIBS ${LINUX_GL_LIBS} -ldl)
    list(APPEND IMGUI_LIBS ${LINUX_GL_LIBS} -ldl)
    list(APPEND FLAGS -DUSE_EPOLL)
    if (Fontconfig_FOUND)
        list(APPEND FLAGS -DUSE_FONTCONFIG)
        list(APPEND LIBS Fontconfig::Fontconfig)
        list(APPEND SOURCES fonts.cpp)
    endif()
elseif (APPLE)
    list(APPEND LIBS -framework OpenGL -framework Cocoa -framework IOKit -framework CoreVideo -L/usr/local/lib -L/opt/local/lib)
    list(APPEND INCLUDES /usr/local/include /opt/local/include)
elseif (WIN32)
    list(APPEND LIBS -lgdi32 -lopengl32 -limm32)
endif()

## ---------------------------------------------------------------------
## PROJECT
## ---------------------------------------------------------------------

add_library(imgui STATIC ${IMGUI_SOURCES})
target_include_directories(imgui PRIVATE ${INCLUDES})
target_link_libraries(imgui PRIVATE ${IMGUI_LIBS})
target_compile_definitions(imgui PRIVATE ${DEFINITIONS})
# dear imgui has some fucky wucky with -Wconversion, hence -Wno-conversion
# it seems like compilers may issue a warning for unknown warnings to ignore
target_compile_options(imgui PRIVATE ${FLAGS} -Wno-unknown-warning-option -Wno-conversion)

set(NFD_PORTAL ON)
add_subdirectory(thirdparty/nativefiledialog-extended)

add_executable(${PROJECT_NAME} ${SOURCES})
set_target_properties(${PROJECT_NAME}
    PROPERTIES
        CXX_STANDARD 20
        CXX_STANDARD_REQUIRED YES
        CXX_EXTENSIONS NO
)
target_include_directories(${PROJECT_NAME} PRIVATE ${INCLUDES})
target_link_libraries(${PROJECT_NAME} PRIVATE ${LIBS})
target_compile_definitions(${PROJECT_NAME} PRIVATE ${DEFINITIONS})
target_compile_options(${PROJECT_NAME} PRIVATE ${FLAGS})
