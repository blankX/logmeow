#include <iomanip>
#include <sstream>
#include <system_error>
#include <imgui/imgui.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <imgui/imgui_internal.h>
#pragma GCC diagnostic pop

#include "misc.h"

std::string quote(const std::string& str) {
    std::stringstream ss;
    ss << std::quoted(str);
    return ss.str();
}

void throw_system_error(int err, const char* what) {
    if (err == ENOMEM) {
        throw std::bad_alloc();
    }
    throw std::system_error(err, std::generic_category(), what);
}

void throw_system_error(int err, std::string what) {
    if (err == ENOMEM) {
        throw std::bad_alloc();
    }
    throw std::system_error(err, std::generic_category(), std::move(what));
}

void throw_system_error(const char* what) {
    throw_system_error(errno, what);
}

void throw_system_error(std::string what) {
    throw_system_error(errno, std::move(what));
}

void ImGui::TextUnformatted(const std::string& str) {
    ImGui::TextUnformatted(str.data(), &str.data()[str.size()]);
}

bool ImGui::RedButton(const char* label) {
    ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.0f, 0.6f, 0.6f));
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.0f, 0.7f, 0.7f));
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.0f, 0.8f, 0.8f));

    bool res = ImGui::Button(label);

    ImGui::PopStyleColor(3);
    return res;
}

bool ImGui::Button(const char* label, bool enabled) {
    if (!enabled) {
        ImGui::BeginDisabled();
    }

    bool res = ImGui::Button(label) && enabled;

    if (!enabled) {
        ImGui::EndDisabled();
    }
    return res;
}

bool ImGui::ArrowButton(const char* label, ImGuiDir dir, bool enabled) {
    if (!enabled) {
        ImGui::BeginDisabled();
    }

    bool res = ImGui::ArrowButton(label, dir) && enabled;

    if (!enabled) {
        ImGui::EndDisabled();
    }
    return res;
}

bool ImGui::BeginWithCloseShortcut(const char* label, bool* p_open, ImGuiWindowFlags flags) {
    bool res = ImGui::Begin(label, p_open, flags);

    if (p_open && ImGui::Shortcut(ImGuiMod_Shortcut | ImGuiKey_W, 0, ImGuiInputFlags_Repeat)) {
        *p_open = false;
        return false;
    }
    return res;
}

bool ImGui::IsKeyPressed(ImGuiKeyChord key_chord, bool repeat) {
    return ImGui::Shortcut(key_chord, 0, repeat ? ImGuiInputFlags_Repeat : 0);
}
