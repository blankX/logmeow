#pragma once

#include <string>
void log(std::string message); // forward declaration from log.h

#define IM_ASSERT_USER_ERROR(expr,msg) do { if (!(expr)) { log(msg); } } while (0);
#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#define IMGUI_ENABLE_FREETYPE

#ifndef NDEBUG
    #define IMGUI_DEBUG_PARANOID
#else
    #define IMGUI_DISABLE_DEMO_WINDOWS
    #define IMGUI_DISABLE_DEBUG_TOOLS
#endif
