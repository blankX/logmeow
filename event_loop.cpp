#include <imgui/imgui.h>

#include "log.h"
#include "config.h"
#include "filters.h"
#include "logcat_entry.h"
#include "logcat_thread.h"

#include "windows/logs.h"
#include "windows/settings.h"
#include "windows/main.h"
#ifndef NDEBUG
    #include "windows/debug.h"
#endif

static inline void check_for_logcat_items(LogcatThread& logcat_thread, LogcatEntries& logcat_entries) {
    LogcatThreadItem* logcat_thread_item;

    while ((logcat_thread_item = logcat_thread.atomic_ring_buffer.get())) {
        if (LogEntry* log_entry = std::get_if<LogEntry>(logcat_thread_item)) {
            log(std::move(*log_entry), false);
        } else if (LogcatEntry* logcat_entry = std::get_if<LogcatEntry>(logcat_thread_item)) {
            logcat_entries.push_back(std::move(*logcat_entry));
        } else {
            throw std::runtime_error("Cannot handle all possible logcat thread item variants");
        }
        logcat_thread.atomic_ring_buffer.increment_read();
    }
}


void event_loop(ImFont* monospace_font, Config& active_config, LogcatThread& logcat_thread) {
    static Config inactive_config;
    static bool show_settings_window = false;
    static bool show_logs_window = false;
    static size_t log_entries_read = 0;
    static LogcatEntries logcat_entries(active_config);

    check_for_logcat_items(logcat_thread, logcat_entries);

#ifndef NDEBUG
    debug_window(logcat_thread, logcat_entries);
#endif

    if (show_settings_window) {
        settings_window(monospace_font, active_config, inactive_config, logcat_entries, &show_settings_window);
    }

    if (show_logs_window) {
        bool autoscrolling = false;
        logs_window(monospace_font, &autoscrolling, &show_logs_window);
        if (autoscrolling) {
            log_entries_read = log_entries.size();
        }
    }

    // log_entries must not be mutated until the show logs button
    main_window(log_entries_read == log_entries.size(), monospace_font, logcat_thread, logcat_entries,
        active_config, inactive_config,
        &show_settings_window, &show_logs_window);
}
