#pragma once

struct ImVec2; // forward declaration from imgui/imgui.h

// The following code is slightly modified public domain code from https://github.com/thedmd
// All modifications have an inline comment with "[CUSTOM]"
// Public domain claim from https://github.com/ocornut/imgui/issues/1496#issuecomment-1287772456
// Code from https://github.com/ocornut/imgui/issues/1496#issuecomment-655048353

namespace ImGui { // [CUSTOM]
void BeginGroupPanel(const ImVec2& size = ImVec2(-1.0f, -1.0f)); // [CUSTOM]
void EndGroupPanel();
}; // namespace ImGui // [CUSTOM]
