#pragma once

#include <string>
#include <utility>

std::pair<std::string, std::string> get_fonts();
